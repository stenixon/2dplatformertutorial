﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
	public GameObject player;
	public GameObject self;
	
	private PlayerPlatformerController playerScript;
	
	void Start() {
		
	playerScript = player.GetComponent<PlayerPlatformerController>();
	}
	
	void OnCollisionEnter2D(Collision2D other){
		playerScript.DoorOpen();
		
		Destroy(self);
		//this.Destroy;
 
	}

}
